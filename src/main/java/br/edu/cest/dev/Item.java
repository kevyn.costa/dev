package br.edu.cest.dev;

public class Item {
	private String title;
	private String publiser;
	private String yearpubliser;
	private String isbn;
	private String price;
	
	protected Item (String title, 
			String publiser, 
			String yearpubliser,
			String isbn,
			String price) {
		
	
	this.title=title;
	this.publiser=publiser;
	this.yearpubliser=yearpubliser;
	this.isbn=isbn;
	this.price=price;
	
	}
	
	public String Display() {
		StringBuffer sb = new StringBuffer();
		sb.append("Título: " + getTitle());
		sb.append("\nPublicador: " + getPubliser());
		sb.append("\nAno publicado: " + getYearpubliser());
		sb.append("\nISBN: " + getIsbn());
		sb.append("\nPreço: " + getPrice());
		return sb.toString();
	}
	
	
	
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getPubliser() {
		return publiser;
	}
	public void setPubliser(String publiser) {
		this.publiser = publiser;
	}
	public String getYearpubliser() {
		return yearpubliser;
	}
	public void setYearpubliser(String yearpubliser) {
		this.yearpubliser = yearpubliser;
	}
	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	
}


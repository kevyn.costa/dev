package br.edu.cest.dev;

	public class Book extends Item {
		private String author;
		private String edition;
		private String volume;
		
		
		
		

		public Book(String title, String author, String edition, String volume) {
			
			super("Zorro: Começa a Lenda" 
					, "Isabel Allende"
					, "2005"
					, "9780007201969"
					,"39,00");
			// TODO Auto-generated constructor stub
		}
		
		@Override
		public String Display() {
		StringBuffer sb = new StringBuffer();
		
		sb.append("Título: " + getTitle());
		sb.append("\nPublicador: " + getPubliser());
		sb.append("\nAno publicado: " + getYearpubliser());
		sb.append("\nISBN: " + getIsbn());
		sb.append("\nPreço: " + getPrice());
		sb.append("\nAutor: " + getAuthor());
		sb.append("\nEdição: " + getEdition());
		sb.append("\nVolume: " + getVolume());
		return sb.toString();
		}
		


		public String getAuthor() {
			return author;
		}



		public void setAuthor(String author) {
			this.author = author;
		}



		public String getEdition() {
			return edition;
		}



		public void setEdition(String edition) {
			this.edition = edition;
		}



		public String getVolume() {
			return volume;
		}



		public void setVolume(String volume) {
			this.volume = volume;
		}		
			
	}
